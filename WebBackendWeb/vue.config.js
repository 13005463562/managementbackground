module.exports = {
    // baseUrl: './',
    // assetsDir: 'static',
    // productionSourceMap: false,
    devServer: {
        port: 8081,
        proxy: {
            '/api': {
                target: 'http://182.61.148.14:8550',
                changeOrigin: true,
                pathRewrite: {
                    '^/api': '/api'
                }
            }
        }
    }
};
