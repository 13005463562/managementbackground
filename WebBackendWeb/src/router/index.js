import Vue from 'vue';
import Router from 'vue-router';

Vue.use(Router);

export default new Router({
    routes: [
        {
            path: '/',
            redirect: '/main'
        },
        {
            path: '/',
            component: () => import('../components/common/Home.vue'),
            meta: { title: '稿件管理' },
            children: [
                {
                    path: '/main',
                    component: () => import('../components/page/Manuscript.vue'),
                    meta: { title: '稿件管理' }
                }
            ]
        },
        {
            path: '/login',
            component: () => import('../components/page/Login.vue'),
            meta: { title: '登录' }
        },
        {
            path: '*',
            redirect: '/404'
        }
    ]
});
