﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;

namespace Application.Commons.Impl
{
    public class CurrentUserSession : ICurrentUserSession
    {
        private readonly IHttpContextAccessor _httpContextAccessor;

        public CurrentUserSession(
            IHttpContextAccessor httpContextAccessor
            )
        {
            _httpContextAccessor = httpContextAccessor;
        }

        /// <summary>
        /// 获取Token中存储的用户Id
        /// </summary>
        /// <returns></returns>
        public int CurrentId()
        {
            string bearer = _httpContextAccessor.HttpContext.Request.Headers["Authorization"].FirstOrDefault();
            if (string.IsNullOrEmpty(bearer) || !bearer.Contains("Bearer")) 
                return 0;
            string[] jwt = bearer.Split(' ');
            var tokenObj = new JwtSecurityToken(jwt[1]);

            var claim = tokenObj.Claims.FirstOrDefault(c => c.Type == "UserId");
            if (claim == null || string.IsNullOrEmpty(claim.Value))
            {
                return 0;
            }

            return int.Parse(claim.Value);
        }

      

    }
}
