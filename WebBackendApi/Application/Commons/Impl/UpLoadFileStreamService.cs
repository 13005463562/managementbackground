﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using Model.Options;
using Model.ViewModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace Application.Commons.Impl
{
    public class UpLoadFileStreamService: IUpLoadFileStreamService
    {
        private readonly UploadFileOptions _uploadFileOptions;
        public UpLoadFileStreamService(IOptions<UploadFileOptions> uploadFileOptions)
        {
            _uploadFileOptions = uploadFileOptions.Value;
        }
        /// <summary>
        /// 上传文件，需要自定义上传的路径
        /// </summary>
        /// <param name="file">文件接口对象</param>
        /// <param name="path">需要自定义上传的路径</param>
        /// <returns></returns>
        public  async Task<BaseResponse<string>> UploadWriteFileAsync(IFormFile file)
        {
            //检测文件是否符合要求
            var checkResult = CheckFile(file);
            if(!checkResult.Item1)
            {
                return new BaseResponse<string>() { Code=BaseResponseCode.Failed,Message=checkResult.Item2};
            }
            string fileOrginname = file.FileName;//新建文本文档.txt  原始的文件名称
            string fileExtention = Path.GetExtension(fileOrginname);
            string cdipath = Directory.GetCurrentDirectory();
            string fileupName = Guid.NewGuid().ToString("d") + fileExtention;
            string directory = $"{cdipath}\\{ _uploadFileOptions.BasePath}";
            string upfilePath = Path.Combine(directory, fileupName);
           
            using (FileStream fileStream = new FileStream(upfilePath, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.ReadWrite, (int)file.Length))
            {
                await file.CopyToAsync(fileStream);
            }
            return new BaseResponse<string>() { Result=$"{_uploadFileOptions.FileUrlBase}/{_uploadFileOptions.BasePath}/{fileupName}",Code = BaseResponseCode.Succeed };
        }

        /// <summary>
        /// 检测文件
        /// </summary>
        /// <param name="formFiles"></param>
        /// <returns></returns>
        private (bool,string) CheckFile(IFormFile file)
        {
            var suffix = Path.GetExtension(file.FileName);//提取上传的文件文件后缀
            var fileSize = file.Length;
            if (fileSize >= this._uploadFileOptions.MaxSize)
            {
                return (false,"文件过大");
            }
            if (!this._uploadFileOptions.FileTypes.Contains(suffix))//检查文件格式
            {
                return (false, "文件类型不合法。");
            }

            return (true,"");
        }
    }
}
