﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Commons
{
    public interface ICurrentUserSession
    {
        int CurrentId();
    }
}
