﻿using Microsoft.AspNetCore.Http;
using Model.ViewModel;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Application.Commons
{
    public interface IUpLoadFileStreamService
    {
        /// <summary>
        /// 上传文件
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        Task<BaseResponse<string>> UploadWriteFileAsync(IFormFile file);
    }
}
