﻿using Model.ViewModel;
using Model.ViewModel.Po;
using Model.ViewModel.Vo;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Application.Account
{
    public interface IAccountService
    {
        /// <summary>
        /// 登录验证
        /// </summary>
        /// <param name="loginPo"></param>
        /// <returns></returns>
        Task<BaseResponse<LoginVo>> Authenticate(LoginPo loginPo);
    }
}
