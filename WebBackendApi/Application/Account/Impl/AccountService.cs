﻿using Core.Domain.Account;
using Core.Domain.Account.Repositories;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Model.Options;
using Model.ViewModel;
using Model.ViewModel.Po;
using Model.ViewModel.Vo;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Application.Account.Impl
{
    public class AccountService:IAccountService
    {
       
        private readonly IUserRepository _userRepository;
        private readonly IUserRoleRepository _userRoleRepository;
        private readonly JwtOptions _jwtOptions;
        public AccountService(IUserRepository userRepository, IOptions<JwtOptions> jwtOptions, IUserRoleRepository userRoleRepository)
        {
            _userRepository = userRepository;
            _jwtOptions = jwtOptions.Value;
            _userRoleRepository=userRoleRepository;
    }
        /// <summary>
        /// 登录验证
        /// </summary>
        /// <param name="loginPo"></param>
        /// <returns></returns>
        public async Task<BaseResponse<LoginVo>> Authenticate(LoginPo loginPo)
        {
            var user = await _userRepository.One(user => user.Account == loginPo.Account && user.Password == StringToMD5(loginPo.Password));
            if (user == null)
            {
                return new BaseResponse<LoginVo>() { Code = BaseResponseCode.Failed, Message = "账号不存在或密码错误" };
            }
            //获取角色
            var userRoles = await _userRoleRepository.GetListBySpec(userRole => userRole.UserId == user.Id);
            List<string> roleName = new List<string>();
            userRoles.ForEach(ur =>{
                roleName.Add(ur.Role.RoleName);
            });
            //获取jwt 并返回
            string token= CreateJwtToken(user, roleName);
            await _userRepository.SaveChanges();
            return new BaseResponse<LoginVo>() { Result=new LoginVo() { Token = token, UserId = user.Id,Role=roleName,Name=user.Name },Code= BaseResponseCode .Succeed};
        }

        /// <summary>
        /// MD5加密
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        private string StringToMD5(string source)
        {
            //32位大写
            using (var md5 = MD5.Create())
            {
                var result = md5.ComputeHash(Encoding.UTF8.GetBytes(source));
                var strResult = BitConverter.ToString(result);
                string result3 = strResult.Replace("-", "");
                return result3;
            }
        }
        /// <summary>
        /// 生成token
        /// </summary>
        /// <param name="user"></param>
        /// <param name="roleNames"></param>
        /// <returns></returns>
        private  string CreateJwtToken(User user,List<string> roleNames)
        {
            var signingAlgorithm = SecurityAlgorithms.HmacSha256;
            var claims = new List<Claim>
            {
                new Claim("UserId", $"{user.Id}"),
            };

            foreach (var roleName in roleNames)
            {
                var roleClaim = new Claim(ClaimTypes.Role, roleName);
                claims.Add(roleClaim);
            }

            var secretByte = Encoding.UTF8.GetBytes(_jwtOptions.SecurityKey);
            var signingkey = new SymmetricSecurityKey(secretByte);
            var signingCredentials = new SigningCredentials(signingkey, signingAlgorithm);

            var token = new JwtSecurityToken(
                issuer: _jwtOptions.ValidIssuer,
                audience: _jwtOptions.ValidAudience,
                claims,
                notBefore: DateTime.UtcNow,
                expires: DateTime.UtcNow.AddMinutes(_jwtOptions.ExpireMinutes),
                signingCredentials
            );

            return new JwtSecurityTokenHandler().WriteToken(token);
        }
    }
}
