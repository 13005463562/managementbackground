﻿using Model.ViewModel;
using Model.ViewModel.Po;
using Model.ViewModel.Vo;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Application.Manuscripts
{
    public interface IManuscriptService
    {
        /// <summary>
        /// 分页查询
        /// </summary>
        /// <param name="loginPo"></param>
        /// <returns></returns>
        Task<BaseResponse<ManuscriptListVo>> Page(ManuscriptListPo po);
        /// <summary>
        /// 发布
        /// </summary>
        /// <param name="po"></param>
        /// <returns></returns>
        Task<BaseResponse> Publish(ManuscriptPublishPo po);
        /// <summary>
        /// 批量打分
        /// </summary>
        /// <param name="po"></param>
        /// <returns></returns>
        Task<BaseResponse> BatchScore(ScorePo po);
    }
}
