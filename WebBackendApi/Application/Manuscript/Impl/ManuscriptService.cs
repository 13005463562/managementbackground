﻿using Application.Commons;
using Core.Domain.Account.Repositories;
using Core.Domain.Manage;
using Core.Domain.Manage.Repositories;
using Model.Enum;
using Model.ViewModel;
using Model.ViewModel.Po;
using Model.ViewModel.Vo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Application.Manuscripts.Impl
{
    public class ManuscriptService : IManuscriptService
    {
        private readonly IManuscriptRepository _manuscriptRepository;
        private readonly IUserRepository _userRepository;
        private readonly ICurrentUserSession _currentUserSession;
        public ManuscriptService(IManuscriptRepository manuscriptRepository,IUserRepository userRepository, ICurrentUserSession currentUserSession)
        {
            _manuscriptRepository = manuscriptRepository;
            _userRepository = userRepository;
            _currentUserSession = currentUserSession;
        }
        /// <summary>
        /// 分页
        /// </summary>
        /// <param name="Po"></param>
        /// <returns></returns>
        public async Task<BaseResponse<ManuscriptListVo>> Page(ManuscriptListPo Po)
        {
            //var userId = _currentUserSession.CurrentId();
            Expression<Func<Manuscript, bool>> expression = c => !c.IsDeleted;
            if (Po.PublistStartTime != null)
            {
                expression = expression.And(c => c.PublishTime >= Po.PublistStartTime);
            }
            if (Po.PublistEndTime != null)
            {
                expression = expression.And(c => c.PublishTime <= Po.PublistEndTime.Value.AddDays(1));
            }
            var list = await _manuscriptRepository.Page(expression, c => c.PublishTime, Po.PageSize??10, Po.PageIndex??1, false);
            var data = new ManuscriptListVo()
            {
                TotalCount = await _manuscriptRepository.RecordCount(expression),
                data = list.Select(m => new ManuscriptVo
                {
                    Id=m.Id,
                    Title = m.Title,
                    Author = m.Author,
                    PublishTime = m.PublishTime,
                    PublishPlatform = (PublishPlatformEnum)m.PublishPlatform,
                    Link = m.Link,
                    Type = (ManuscriptTypeEnum)m.Type,
                    WordsNum = m.WordsNum,
                    IsExternalSolicitation = m.IsExternalSolicitation,
                    Score = m.Score
                }).ToList()
            };
            return new BaseResponse<ManuscriptListVo>() { Code = BaseResponseCode.Succeed, Result = data };
        }
        /// <summary>
        /// 发布
        /// </summary>
        /// <param name="po"></param>
        /// <returns></returns>
        public async Task<BaseResponse> Publish(ManuscriptPublishPo po)
        {
            var userId = _currentUserSession.CurrentId();
            var author =await _userRepository.GetById(userId);
            if(author==null)
            {
                return new BaseResponse() { Code = BaseResponseCode.Failed, Message = $"用户不存在！" };
            }
            Manuscript manuscript = new Manuscript()
            {
                Title = po.Title,
                AuthorId = userId,
                Author= author.Name,
                PublishTime = DateTime.Now,
                PublishPlatform =(int)po.PublishPlatform,
                Content=po.Content,
                Link = po.Link,
                Type = (int)po.Type,
                WordsNum = po.WordsNum,
                IsExternalSolicitation = po.IsExternalSolicitation,
                IsDeleted=false
            };

            await _manuscriptRepository.Add(manuscript);
            await _manuscriptRepository.SaveChanges();
            return new BaseResponse() { Code=BaseResponseCode.Succeed};
        }
        /// <summary>
        /// 打分
        /// </summary>
        /// <param name="po"></param>
        /// <returns></returns>
        public async Task<BaseResponse> BatchScore(ScorePo po)
        {
            var userId = _currentUserSession.CurrentId();
            if(!po.ManuscriptIds.Any())
            {
                return new BaseResponse() { Code = BaseResponseCode.Failed, Message = $"请选择稿件" };
            }
            //检查这些稿件是否已经打分 避免其他账号同时打分
            var titles= _manuscriptRepository.GetTitleBySpec(c=> po.ManuscriptIds.Contains(c.Id) && c.ScoreUserId!=null);
            if(titles.Any())
            {
                return new BaseResponse() { Code = BaseResponseCode.Failed,Message=$"打分失败，{string.Join('，', titles)} 等稿件已经打分!" };
            }
            //批量更新
            var updateSql = $" update Manuscripts set score={po.Score} , ScoreUserId={userId} where Id in ( { string.Join(',', po.ManuscriptIds) } ) ;";
            await _manuscriptRepository.ExcuteSql(updateSql);
            return new BaseResponse() { Code = BaseResponseCode.Succeed };
        }
    }
}
