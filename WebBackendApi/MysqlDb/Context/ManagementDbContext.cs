﻿using Core.Domain.Account;
using Core.Domain.Manage;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace MysqlDb.Context
{
    public class ManagementDbContext:DbContext
    {
        /// <summary>
        ///  实例化
        /// </summary>
        /// <param name="options">di注入参数</param>
        public ManagementDbContext(DbContextOptions<ManagementDbContext> options) : base(options) { }


        /// <summary>
        /// 映射关系
        /// </summary>
        /// <param name="modelBuilder">设置数据库映射对像</param>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            //配置外键
            modelBuilder.Entity<UserRole>(userRole =>
            {
                userRole.HasKey(ur => new { ur.UserId, ur.RoleId });
                userRole.HasOne(ur => ur.Role)
                    .WithMany(r => r.UserRoles)
                    .HasForeignKey(ur => ur.RoleId)
                    .IsRequired().OnDelete(DeleteBehavior.NoAction);

                userRole.HasOne(ur => ur.User)
                    .WithMany(r => r.UserRoles)
                    .HasForeignKey(ur => ur.UserId)
                    .IsRequired().OnDelete(DeleteBehavior.NoAction);
            });
        }

        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<Role> Roles { get; set; }
        public virtual DbSet<UserRole> UserRoles { get; set; }
        public virtual DbSet<Manuscript> Manuscripts { get; set; }
    }
}
