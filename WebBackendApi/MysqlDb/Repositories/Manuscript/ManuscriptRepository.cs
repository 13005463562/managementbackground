﻿using Core.Domain.Manage;
using Core.Domain.Manage.Repositories;
using MysqlDb.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace MysqlDb.Repositories
{
    public class ManuscriptRepository : MysqlDbRepositoryBase<Manuscript>, IManuscriptRepository
    {
        public ManuscriptRepository(ManagementDbContext dbContext) : base(dbContext)
        {
        }
        /// <summary>
        /// 获取指定条件的稿件标题
        /// </summary>
        /// <param name="spec"></param>
        /// <returns></returns>
        public List<string> GetTitleBySpec(Expression<Func<Manuscript, bool>> spec)
        {
            return base._dbContext.Manuscripts.Where(spec).Select(t => t.Title).ToList();
        }
    }
}
