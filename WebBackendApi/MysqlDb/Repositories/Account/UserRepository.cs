﻿using Core.Domain.Account;
using Core.Domain.Account.Repositories;
using MysqlDb.Context;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace MysqlDb.Repositories.Account
{
    public class UserRepository : MysqlDbRepositoryBase<User>,IUserRepository
    {
        public UserRepository(ManagementDbContext dbContext) : base(dbContext)
        {
        }
    }
}
