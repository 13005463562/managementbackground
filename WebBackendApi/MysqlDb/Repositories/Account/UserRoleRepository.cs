﻿using Core.Domain.Account;
using Core.Domain.Account.Repositories;
using MysqlDb.Context;
using System;
using System.Collections.Generic;
using System.Text;

namespace MysqlDb.Repositories.Account
{
    public class UserRoleRepository : MysqlDbRepositoryBase<UserRole>, IUserRoleRepository
    {
        public UserRoleRepository(ManagementDbContext dbContext) : base(dbContext)
        {
        }
    }
}
