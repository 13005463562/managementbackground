﻿using Core.Domain.Account.Repositories;
using Core.Domain.Manage.Repositories;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using MysqlDb.Context;
using MysqlDb.Repositories;
using MysqlDb.Repositories.Account;
using Pomelo.EntityFrameworkCore.MySql.Infrastructure;
using System;
using System.Collections.Generic;
using System.Text;

namespace MysqlDb
{
    public static class RepositorysInjectionModule
    {
        /// <summary>
        /// 配置所有数据库访问
        /// </summary>
        /// <param name="services"></param>
        /// <param name="configuration"></param>
        /// <returns></returns>
        public static IServiceCollection AddRepositorysModule(this IServiceCollection services, IConfiguration configuration)
        {
            string connstr = configuration.GetConnectionString("Default");
            services.AddDbContext<ManagementDbContext>(options =>options.UseMySql(connstr, ServerVersion.Parse("5.7")).UseLazyLoadingProxies().UseQueryTrackingBehavior(QueryTrackingBehavior.TrackAll));

            //services.AddDbContext<ManagementDbContext>(options => options.UseSqlServer(configuration.GetConnectionString("Default"),
            //   sqlOptions =>
            //   {
            //       sqlOptions.EnableRetryOnFailure().CommandTimeout(10);
            //       sqlOptions.EnableRetryOnFailure(maxRetryCount: 0, maxRetryDelay: TimeSpan.FromMilliseconds(1200), errorNumbersToAdd: null);
            //   }).UseLazyLoadingProxies().UseQueryTrackingBehavior(QueryTrackingBehavior.TrackAll));
            services.AddScoped(typeof(IUserRepository), typeof(UserRepository));
            services.AddScoped(typeof(IUserRoleRepository), typeof(UserRoleRepository));
            services.AddScoped(typeof(IManuscriptRepository), typeof(ManuscriptRepository));

            return services;
        }
    }
}
