﻿using Core.Domain;
using Microsoft.EntityFrameworkCore;
using MysqlDb.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace MysqlDb
{
    public class MysqlDbRepositoryBase<TEntity> : IRepository<TEntity> where TEntity: BaseEntity ,new ()
    {
        /// <summary>
        /// 数据集
        /// </summary>
        protected readonly ManagementDbContext _dbContext;

        protected MysqlDbRepositoryBase(ManagementDbContext dbContext)
        {
            _dbContext = dbContext;
        }
        /// <summary>
        /// 插入一条数据
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public async Task<TEntity> Add(TEntity entity)
        {
            await _dbContext.AddAsync(entity);
            return entity;
        }

        /// <summary>
        /// 获取by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<TEntity> GetById(int id)
        {
           return await _dbContext.Set<TEntity>().FindAsync(id);
        }
        /// <summary>
        /// 更新
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public async Task Update(TEntity entity)
        {
             _dbContext.Set<TEntity>().Update(entity);
            await _dbContext.SaveChangesAsync();
        }
        /// <summary>
        /// 查找一个
        /// </summary>
        /// <param name="spec"></param>
        /// <returns></returns>
        public async Task<TEntity> One(Expression<Func<TEntity, bool>> spec)
        {
            //return  _dbContext.Set<TEntity>().FirstOrDefault();
            var queryable = _dbContext.Set<TEntity>().Where(spec);

            var listAsync = await queryable.Take(1).ToListAsync();
            if (listAsync.Any())
                return listAsync.First();
            return null;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public async Task SaveChanges()
        {
            await _dbContext.SaveChangesAsync();
        }
        /// <summary>
        /// 获取列表
        /// </summary>
        /// <param name="spec"></param>
        /// <returns></returns>
        public async Task<List<TEntity>> GetListBySpec(Expression<Func<TEntity, bool>> spec)
        {
            return await _dbContext.Set<TEntity>().Where(spec).ToListAsync();
        }
        /// <summary>
        /// 分页查询
        /// </summary>
        /// <typeparam name="TKey"></typeparam>
        /// <param name="spec"></param>
        /// <param name="orderBy"></param>
        /// <param name="pageSize"></param>
        /// <param name="pageIndex"></param>
        /// <param name="isAsc"></param>
        /// <returns></returns>
        public virtual async Task<IList<TEntity>> Page<TKey>(Expression<Func<TEntity, bool>> spec,
          Expression<Func<TEntity, TKey>> orderBy, int pageSize, int pageIndex, bool isAsc) =>
          isAsc
              ? await _dbContext.Set<TEntity>().Where(spec).OrderBy(orderBy)
                  .Skip(pageSize * (pageIndex - 1)).Take(pageSize).ToListAsync()
              : await _dbContext.Set<TEntity>().Where(spec).OrderByDescending(orderBy)
                  .Skip(pageSize * (pageIndex - 1)).Take(pageSize).ToListAsync();

        /// <summary>
        /// 执行sql
        /// </summary>
        /// <param name="sql"></param>
        /// <returns></returns>
        public async Task<int> ExcuteSql(string sql)
        {
            return await _dbContext.Database.ExecuteSqlRawAsync(sql);
        }
        /// <summary>
        /// 总记录数
        /// </summary>
        /// <returns></returns>
        public async Task<int> RecordCount(Expression<Func<TEntity, bool>> ex)
        {
            return await _dbContext.Set<TEntity>().CountAsync(ex);
        }
    }
}
