﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model.ViewModel
{
    public class BaseResponse<TResult> : BaseResponse where TResult : class
    {
        public TResult Result { get; set; }

        public void IsSuccess(TResult result = null, string message = "")
        {
            Code = BaseResponseCode.Succeed;
            Message = message;
            Result = result;
        }
    }

    public class BaseResponse
    {
        public BaseResponseCode Code { get; set; }

        public string Message { get; set; } = string.Empty;

        public bool Success => Code == BaseResponseCode.Succeed;

    }

    public enum BaseResponseCode : int
    {
        Succeed,
        Failed
    }
}
