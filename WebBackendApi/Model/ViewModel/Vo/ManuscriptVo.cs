﻿using Model.Enum;
using System;
using System.Collections.Generic;
using System.Text;

namespace Model.ViewModel.Vo
{
    public class ManuscriptVo
    {
        /// <summary>
        /// id
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// 标题
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// 作者
        /// </summary>
        public string Author { get; set; }
        /// <summary>
        /// 发布时期
        /// </summary>
        public DateTime PublishTime { get; set; }
        /// <summary>
        /// 发布平台
        /// </summary>
        public PublishPlatformEnum PublishPlatform { get; set; }
        /// <summary>
        /// 链接
        /// </summary>
        public string Link { get; set; }
        /// <summary>
        /// 类型（图文，视频）
        /// </summary>
        public ManuscriptTypeEnum Type { get; set; }
        /// <summary>
        /// 稿件字数
        /// </summary>
        public int WordsNum { get; set; }
        /// <summary>
        /// 是否外部约稿
        /// </summary>
        public bool IsExternalSolicitation { get; set; }
        /// <summary>
        /// 分数（0-100）
        /// </summary>
        public double? Score { get; set; }
    }
}
