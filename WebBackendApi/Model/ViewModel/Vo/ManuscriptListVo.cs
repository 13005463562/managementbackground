﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model.ViewModel.Vo
{
    public class ManuscriptListVo
    {
        /// <summary>
        /// 数据列表
        /// </summary>
        public List<ManuscriptVo> data { get; set; }
        /// <summary>
        /// 总行数
        /// </summary>
        public int TotalCount { get; set; }
    }
}
