﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model.ViewModel.Vo
{
    public class LoginVo
    {
        /// <summary>
        /// 用户id
        /// </summary>
        public int UserId { get; set; }
        /// <summary>
        /// 名字
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 角色
        /// </summary>
        public List<string> Role { get; set; }
        /// <summary>
        /// token
        /// </summary>
        public string Token { get; set; }
    }
}
