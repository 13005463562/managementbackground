﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Model.ViewModel.Po
{
    public class ScorePo
    {
        /// <summary>
        /// 稿件id
        /// </summary>
        [Required]
        public List<int> ManuscriptIds { get; set; }
        /// <summary>
        /// 分数
        /// </summary>
        [Required]
        [Range(0,100,ErrorMessage ="分数在0-100之间")]
        public double Score { get; set; }
    }
}
