﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Model.ViewModel.Po
{
    public class LoginPo
    {
        /// <summary>
        /// 账号
        /// </summary>
        [Required]
        [MaxLength(20, ErrorMessage = "账号长度应小于20")]
        public string Account { get; set; }
        /// <summary>
        /// 密码
        /// </summary>
        [Required]
        public string Password { get; set; }
    }
}
