﻿using Model.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Model.ViewModel.Po
{
    public class ManuscriptPublishPo
    {
        /// <summary>
        /// 标题
        /// </summary>
        [Required]
        public string Title { get; set; }

        /// <summary>
        /// 发布平台(1,newspaper 2. wechat )
        /// </summary>
        [Required]
        public PublishPlatformEnum PublishPlatform { get; set; }
        /// <summary>
        /// 链接
        /// </summary>
        [Required]
        public string Link { get; set; }
        /// <summary>
        /// 类型（1 图文，2 视频）
        /// </summary>
        [Required]
        public ManuscriptTypeEnum Type { get; set; }
        /// <summary>
        /// 内容
        /// </summary>
        [Required]
        public string Content { get; set; }
        /// <summary>
        /// 稿件字数
        /// </summary>
        [Required]
        public int WordsNum { get; set; }
        /// <summary>
        /// 是否外部约稿
        /// </summary>
        [Required]
        public bool IsExternalSolicitation { get; set; }
    }
}
