﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Model.ViewModel.Po
{
    public class ManuscriptListPo
    {
        /// <summary>
        /// 页码 
        /// </summary>
        public int? PageIndex { get; set; }
        /// <summary>
        /// 页size
        /// </summary>
        public int? PageSize { get; set; }
        /// <summary>
        /// 开始时间时间戳
        /// </summary>
        public long? PublistStartTimestamp { get; set; }
        /// <summary>
        /// 开始时间时间戳
        /// </summary>
        public long? PublistEndTimestamp { get; set; }
        /// <summary>
        /// 发布开始日期
        /// </summary>
        public DateTime? PublistStartTime =>  LongTimeStampToDateTime(PublistStartTimestamp);
        /// <summary>
        /// 发布结束日期
        /// </summary>
        public DateTime? PublistEndTime => LongTimeStampToDateTime(PublistEndTimestamp);

        /// <summary>
        /// 13位时间戳（单位：毫秒）转换为DateTime
        /// </summary>
        /// <param name="longTimeStamp">13位时间戳（单位：毫秒）</param>
        /// <returns>DateTime</returns>
        private  DateTime? LongTimeStampToDateTime(long? longTimeStamp)
        {
            if (longTimeStamp == null) return null;
            return new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc).AddMilliseconds(longTimeStamp.Value).ToLocalTime();
        }
    }
}
