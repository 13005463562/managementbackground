﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Options
{
    public class JwtOptions
    {
        /// <summary>
        /// SecurityKey
        /// </summary>
        public string SecurityKey { get; set; }
        /// <summary>
        /// ValidAudience
        /// </summary>
        public string ValidAudience { get; set; }
        /// <summary>
        /// ValidIssuer
        /// </summary>
        public string ValidIssuer { get; set; }
        /// <summary>
        /// ExpireMinutes
        /// </summary>
        public int ExpireMinutes { get; set; }
    }
}
