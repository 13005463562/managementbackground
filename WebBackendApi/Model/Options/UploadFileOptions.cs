﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Options
{
    public class UploadFileOptions
    {
        /// <summary>
        /// 文件类型
        /// </summary>
        public string FileTypes { get; set; }
        /// <summary>
        /// 最大
        /// </summary>
        public long MaxSize { get; set; }
        /// <summary>
        /// 文件目录
        /// </summary>
        public string BasePath { get; set; }
        /// <summary>
        /// url目录
        /// </summary>
        public string FileUrlBase { get; set; }
    }
}
