﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Model.Enum
{
    public enum PublishPlatformEnum
    {
        [Description("报纸")]
        Newspaper=1,
        [Description("微信")]
        Wechat =2
    }
}
