﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Model.Enum
{
    public enum ManuscriptTypeEnum
    {
        [Description("图文")]
        Picture=1,
        [Description("视频")]
        Video =2
    }
}
