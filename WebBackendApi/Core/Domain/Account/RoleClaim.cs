﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Domain.Account
{
    public class RoleClaim:BaseEntity
    {
        /// <summary>
        /// 角色id
        /// </summary>
        public int RoleId { get; set; }
        public  string ClaimType { get; set; }
        public  string ClaimValue { get; set; }
    }
}
