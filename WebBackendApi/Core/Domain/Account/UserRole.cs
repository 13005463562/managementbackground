﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Domain.Account
{
    public class UserRole:BaseEntity
    {
        /// <summary>
        /// userid
        /// </summary>
        public int UserId { get; set; }
        /// <summary>
        /// 角色id
        /// </summary>
        public int RoleId { get; set; }
        /// <summary>
        /// 用户
        /// </summary>
        public virtual User User { get; set; }
        /// <summary>
        /// 角色
        /// </summary>
        public virtual Role Role { get; set; }
    }
}
