﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Domain.Account.Repositories
{
    public interface IUserRoleRepository : IRepository<UserRole>
    {
    }
}
