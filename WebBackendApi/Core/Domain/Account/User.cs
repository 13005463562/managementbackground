﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Core.Domain.Account
{
    public class User: BaseEntity
    {
        /// <summary>
        /// 账号
        /// </summary>
        [MaxLength(20)]
        public string Account { get; set; }
        /// <summary>
        /// 姓名
        /// </summary>
        [MaxLength(20)]
        public string Name { get; set; }

        /// <summary>
        /// 密码
        /// </summary>
        [MaxLength(32)]
        public string Password { get; set; }
        /// <summary>
        /// 角色
        /// </summary>
        public virtual ICollection<UserRole> UserRoles { get; set; }
    }
}
