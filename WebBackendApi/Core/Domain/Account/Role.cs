﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Domain.Account
{
    public class Role:BaseEntity
    {
        /// <summary>
        /// 角色名
        /// </summary>
        public string RoleName { get; set; }
        /// <summary>
        /// 角色
        /// </summary>
        public virtual ICollection<UserRole> UserRoles { get; set; }
        /// <summary>
        /// 角色claim
        /// </summary>
        //public virtual ICollection<RoleClaim> RoleClaims { get; set; }
    }
}
