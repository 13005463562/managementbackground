﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Core.Domain
{
    public interface IRepository<T>
    {
        /// <summary>
        /// 获取byid
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<T> GetById(int id);
        /// <summary>
        /// 插入
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        Task<T> Add(T entity);
        /// <summary>
        /// 更新
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        Task Update(T entity);
        /// <summary>
        /// 查询一条数据
        /// </summary>
        /// <param name="spec"></param>
        /// <returns></returns>
        Task<T> One(Expression<Func<T, bool>> spec);
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        Task SaveChanges();
        /// <summary>
        /// 获取了列表
        /// </summary>
        /// <param name="spec"></param>
        /// <returns></returns>
        Task<List<T>> GetListBySpec(Expression<Func<T, bool>> spec);
        /// <summary>
        /// 分页
        /// </summary>
        /// <typeparam name="TKey"></typeparam>
        /// <param name="spec"></param>
        /// <param name="orderBy"></param>
        /// <param name="pageSize"></param>
        /// <param name="pageIndex"></param>
        /// <param name="isAsc"></param>
        /// <returns></returns>
        Task<IList<T>> Page<TKey>(Expression<Func<T, bool>> spec,
          Expression<Func<T, TKey>> orderBy, int pageSize, int pageIndex, bool isAsc);
        /// <summary>
        /// 执行sql
        /// </summary>
        /// <param name="sql"></param>
        /// <returns></returns>
        Task<int> ExcuteSql(string sql);
        /// <summary>
        /// 总记录数
        /// </summary>
        /// <returns></returns>
        Task<int> RecordCount(Expression<Func<T, bool>> ex);

    }
}
