﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Core.Domain.Manage.Repositories
{
    public interface IManuscriptRepository : IRepository<Manuscript>
    {
        /// <summary>
        /// 获取标题
        /// </summary>
        /// <param name="spec"></param>
        /// <returns></returns>
        List<string> GetTitleBySpec(Expression<Func<Manuscript, bool>> spec);
    }
}
