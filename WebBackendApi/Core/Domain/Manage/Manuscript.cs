﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Core.Domain.Manage
{
    public class Manuscript:BaseEntity
    {
        /// <summary>
        /// 标题
        /// </summary>
        [MaxLength(30)]
        public string Title { get; set; }
        /// <summary>
        /// 作者
        /// </summary>
        [MaxLength(20)]
        public string Author { get; set; }
        /// <summary>
        /// 作者id
        /// </summary>
        public int AuthorId { get; set; }
        /// <summary>
        /// 发布时期
        /// </summary>
        public DateTime PublishTime { get; set; }
        /// <summary>
        /// 发布平台
        /// </summary>
        public int PublishPlatform { get; set; }
        /// <summary>
        /// 链接
        /// </summary>
        [MaxLength(200)]
        public string Link { get; set; }
        /// <summary>
        /// 类型（图文，视频）
        /// </summary>
        public int Type { get; set; }
        /// <summary>
        /// 内容
        /// </summary>
        public string Content { get; set; }
        /// <summary>
        /// 稿件字数
        /// </summary>
        public int WordsNum { get; set; }
        /// <summary>
        /// 是否外部约稿
        /// </summary>
        public bool IsExternalSolicitation { get; set; }
        /// <summary>
        /// 分数（0-100）
        /// </summary>
        public double? Score { get; set; }
        /// <summary>
        /// 打分人id
        /// </summary>
        public int? ScoreUserId { get; set; }
        /// <summary>
        /// 打分日期
        /// </summary>
        public DateTime? ScoreTime { get; set; }
        /// <summary>
        /// 是否删除
        /// </summary>
        public bool IsDeleted { get; set; }
    }
}
