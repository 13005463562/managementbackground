﻿using Application.Account;
using Application.Commons;
using Application.Manuscripts;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Model.ViewModel;
using Model.ViewModel.Po;
using Model.ViewModel.Vo;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace WebBackendApi.Controllers
{
    [Route("api/[controller]")]
    [Authorize(AuthenticationSchemes = "Bearer")]
    public class ManuscriptController : ControllerBase
    {
        private readonly IManuscriptService _manuscriptService;
        private readonly IUpLoadFileStreamService _upLoadFileStreamService;
        public ManuscriptController(IManuscriptService manuscriptService, IUpLoadFileStreamService upLoadFileStreamService)
        {
            _manuscriptService = manuscriptService;
            _upLoadFileStreamService = upLoadFileStreamService;
        }
        /// <summary>
        /// 稿件列表
        /// </summary>
        /// <param name="po"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("Page")]
        public async Task<BaseResponse<ManuscriptListVo>> Page([FromQuery] ManuscriptListPo po)
        {
            return await _manuscriptService.Page(po);
        }
        /// <summary>
        /// 发布稿件
        /// </summary>
        /// <param name="po"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("Publish")]
        [Authorize(Roles = "write")]
        public async Task<BaseResponse> Publish([FromBody] ManuscriptPublishPo po)
        {
            return await _manuscriptService.Publish(po);
        }
        /// <summary>
        /// 批量打分
        /// </summary>
        /// <param name="po"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("Score")]
        [Authorize(Roles = "score")]
        public async Task<BaseResponse> BatchScore([FromBody] ScorePo po)
        {
            return await _manuscriptService.BatchScore(po);
        }

 
        /// <summary>
        /// 上传文件
        /// </summary>
        /// <param name="formFile"></param>
        /// <param name="path">路劲</param>
        /// <returns></returns>
        [HttpPost]
        [Route("UpFile")]
        public async Task<BaseResponse<string>> UploadFile([FromForm] IFormFile file)
        {
           return await _upLoadFileStreamService.UploadWriteFileAsync(file);
        }
       
    }
}
