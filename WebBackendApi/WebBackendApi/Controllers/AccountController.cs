﻿using Application.Account;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Model.ViewModel;
using Model.ViewModel.Po;
using Model.ViewModel.Vo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebBackendApi.Controllers
{
    [Route("api/[controller]")]
    public class AccountController : ControllerBase
    {
        private readonly IAccountService _accountService;
        public AccountController(IAccountService accountService)
        {
            _accountService = accountService;
        }
        /// <summary>
        /// 用户登录
        /// </summary>
        /// <param name="po"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("Login")]
        [AllowAnonymous]
        public async Task<BaseResponse<LoginVo>> Login([FromBody]LoginPo po)
        {
            var result=await _accountService.Authenticate(po);
            return result;
        }

        [HttpGet]
        [Route("test")]
        [AllowAnonymous]
        public async Task<string> Test()
        {
            return "test";
        }
    }
}
