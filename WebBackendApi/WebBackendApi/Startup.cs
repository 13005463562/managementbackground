using Application.Account;
using Application.Account.Impl;
using Application.Commons;
using Application.Commons.Impl;
using Application.Manuscripts;
using Application.Manuscripts.Impl;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using Model.Options;
using MysqlDb;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebBackendApi
{
    public class Startup
    {
        public IConfiguration Configuration { get; }
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            //数据库及仓储
            services.AddRepositorysModule(Configuration);

            //服务
            services.AddTransient(typeof(IAccountService), typeof(AccountService));
            services.AddTransient(typeof(IManuscriptService), typeof(ManuscriptService));
            services.AddTransient(typeof(IUpLoadFileStreamService), typeof(UpLoadFileStreamService));
            services.AddTransient(typeof(ICurrentUserSession), typeof(CurrentUserSession));

            services.AddControllers();
            services.AddHttpContextAccessor();
            //添加jwt验证
            var jwtOptions = new JwtOptions();
            services.Configure<JwtOptions>(Configuration.GetSection("JwtOptions"));
            Configuration.Bind("JwtOptions", jwtOptions);
            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                 .AddJwtBearer(p =>
                 {
                     p.TokenValidationParameters = new TokenValidationParameters
                     {
                         ValidateIssuer = true,
                         ValidateAudience = true,
                         ValidateLifetime = true,
                         ClockSkew = TimeSpan.FromSeconds(30),
                         ValidateIssuerSigningKey = true,
                         ValidAudience = jwtOptions.ValidAudience,
                         ValidIssuer = jwtOptions.ValidIssuer,
                         IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(jwtOptions.SecurityKey))
                     };
                 });

            //文件上传相关
            services.Configure<UploadFileOptions>(Configuration.GetSection("UploadFileOptions"));

            //添加全局的授权策略 避免上传的文件被公开访问
            //services.AddAuthorization(options =>
            //{
            //    options.FallbackPolicy = new AuthorizationPolicyBuilder()
            //        .RequireAuthenticatedUser()
            //        .Build();
            //});

            services.AddCors();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }


            app.UseRouting();

            app.UseCors();//启用跨域 放在路由后 认证前

            app.UseAuthentication();
            app.UseAuthorization();
           

            string cdipath = Directory.GetCurrentDirectory();
            string directory = $"{cdipath}/{Configuration.GetSection("UploadFileOptions:BasePath").Value}";
            if (!System.IO.Directory.Exists(directory))
            {
                Directory.CreateDirectory(directory);
            }
            //放在授权后 对文件也授权管理
            app.UseStaticFiles(new StaticFileOptions()
            {
                FileProvider = new PhysicalFileProvider(directory),//指定实际物理路径
                RequestPath = new PathString("/Uploads")//对外的访问路径
            });

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
