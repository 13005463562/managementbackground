# 演示环境
http://news.zlofyao.top/

账号：

写稿人 ：

账号 123 密码 abc

账号 hello 密码 abc

打分人 ：

账号 456 密码 abc

账号 aaa 密码 abc

# 目录
WebBackendApi --后端api

WebBackendWeb --前端web

# WebBackendApi
## 基本环境
.net core 3.1

mysql

## 目录结构
WebBackendApi --api接口层

Application -- service等等

Core -- 实体，仓储接口等等

MysqlDb --  dbcontext 仓储实现 数据迁移 等等

Model  -- viewmodel 等等

## 本地运行

cd MysqlDb

dotnet ef database update

另外需要手动插入用户/角色等信息，脚本后边补上

cd WebBackendApi

dotnet run

## 暂缺的模块
swagger

日志

异常捕获中间件


# WebBackendWeb

## 基本介绍
vue 2.6

登录  /components/page/Login.vue

稿件管理 /components/page/Manuscript.vue

## 本地运行
npm install

npm run dev